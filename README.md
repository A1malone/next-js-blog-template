# Next JS Blog Template

Easy to make your own Next JS Blog Template. This project was made with performance and SEO in mind along with being able to seamlessly be added as an addition to any next project.

## Features:

- SEO Friendly / HTML rich snippet
- Social media sharing
- Tags / like system
- Dashboard - enables CRUD for blog posts
  - Rich text editor

## Mongoose Schema

```javascript
import mongoose from "mongoose";
const { Schema } = mongoose;

const blogSchema = new Schema({
  title: String,
  author: String,
  body: String,
  comments: [{ body: String, date: Date }],
  date: { type: Date, default: Date.now },
  hidden: Boolean,
  meta: {
    votes: Number,
    favs: Number,
    shortDescirption: String,
  },
});
```

## Contributing

Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License

[MIT](https://choosealicense.com/licenses/mit/)
