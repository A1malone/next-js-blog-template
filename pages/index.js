import React from "react";
import Nav from "../components/navbar";
import Spotlight from "../components/BlogUtilities/spotlight";
import Controlposts from "../components/Controlposts";
import Seo from "../components/SEO";

const testpost = [
  {
    title: "This is an example title for this example blog post",
    image: "/assets/2.jpg",
    publishedOn: "January 20, 2020",
    body: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.",
  },
  {
    title: "This is an example title for this example blog post",
    image: "/assets/3.jpg",
    publishedOn: "January 20, 2020",
    body: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.",
  },
  {
    title: "This is an example title for this example blog post",
    image: "/assets/4.jpg",
    publishedOn: "January 20, 2020",
    body: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.",
  },
  {
    title: "This is an example title for this example blog post",
    image: "/assets/3.jpg",
    publishedOn: "January 20, 2020",
    body: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.",
  },
  {
    title: "This is an example title for this example blog post",
    image: "/assets/2.jpg",
    publishedOn: "January 20, 2020",
    body: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.",
  },
  {
    title: "This is an example title for this example blog post",
    image: "/assets/4.jpg",
    publishedOn: "January 20, 2020",
    body: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.",
  },
];

const Landing = () => {
  return (
    <>
      <Seo
        title="The Blog"
        Description="This is a blog template made for next js websites"
        url="inserturlhere.com"
      />
      <Nav />
      <section className="blog-landing">
        <div className="container-fluid blog-padding">
          <h1 className="blog-header">The Blog</h1>
          <Spotlight
            image={"/assets/1.jpg"}
            title={"This Post is a test post to show what it would look like"}
            date={"January 20, 2021"}
            body={
              " Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam."
            }
          />
        </div>
      </section>
      <Controlposts control={true} />
    </>
  );
};

export default Landing;
