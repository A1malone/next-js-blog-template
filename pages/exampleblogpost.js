import React from "react";
import Nav from "../components/navbar";
import Seo from "../components/SEO";
import Header from "../components/BlogUtilities/header";
import Layout from "../components/BlogUtilities/layout";
import Footer from "../components/footer";

const Blogpost = () => {
  return (
    <>
      <Seo
        title="The Blog"
        Description="This is a blog template made for next js websites"
        url="inserturlhere.com"
      />
      <Nav />
      <Header
        header={"This is an example title for this example blog post"}
        author={"Alfred Malone"}
        published={"January 20, 2021"}
      />
      <Layout />
      <Footer />
    </>
  );
};

export default Blogpost;
