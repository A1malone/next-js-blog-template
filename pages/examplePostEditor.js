import React from "react";
import Seo from "../components/SEO";
import dynamic from "next/dynamic";
import "suneditor/dist/css/suneditor.min.css"; // Import Sun Editor's CSS File
import Nav from "../components/navbar";
import Button from "react-bootstrap/Button";

const SunEditor = dynamic(() => import("suneditor-react"), {
  ssr: false,
});
const buttonList = dynamic(() => import("suneditor-react"), {
  ssr: false,
});

const Posteditor = (props) => {
  return (
    <>
      <Seo
        title="Example Post Editor"
        Description="This is the example post editor"
        url="inserturlhere.com"
      />
      <Nav />
      <section id="post-editor" className="container">
        <h1 className="mt-2">Add New Post</h1>

        <form
          className="my-lg-5"
          method="post"
          action="https://alfredmalone.com/blog/PostBlog"
        >
          <div className="form-floating col-sm-12 col-md-7">
            <input
              type="text"
              name="name"
              className="form-control"
              id="floatingInput"
              placeholder="Name*"
            />
            <label htmlFor="floatingInput">Title Name</label>
          </div>
          <SunEditor
            name="postbody"
            placeholder="Please type here..."
            height="50vh"
            setOptions={{
              charCounter: true,
              buttonList: [
                ["undo", "redo"],
                ["font", "fontSize", "formatBlock"],
                [
                  "paragraphStyle",
                  "blockquote",
                  "bold",
                  "underline",
                  "italic",
                  "strike",
                  "subscript",
                  "superscript",
                  "removeFormat",
                ],
                [
                  "fontColor",
                  "hiliteColor",
                  "outdent",
                  "indent",
                  "align",
                  "horizontalRule",
                  "list",
                  "table",
                ],
                [
                  "link",
                  "image",
                  "video",
                  "fullScreen",
                  "showBlocks",
                  "codeView",
                  "preview",
                  "print",
                ],
              ],
              formats: [
                "p",
                "div",
                "blockquote",
                "pre",
                "h1",
                "h2",
                "h3",
                "h4",
                "h5",
                "h6",
                // "blockquote": range format, "pre": free format, "Other tags": replace format
              ],
              Custom: [
                {
                  tag: "div", // Tag name
                  name: "Custom div" || null, // default: tag name
                  command: "replace" || "range" || "free", // default: "replace"
                  class:
                    "__se__format__replace_xxx" ||
                    "__se__format__range_xxx" ||
                    "__se__format__free_xxx" ||
                    "__se__format__free__closure_xxx",
                  // Class names must always begin with "__se__format__(replace, range, free)_"
                },
              ],
              charCounter: true,
            }}
          />
          <Button variant="dark" className="mt-3">
            Post
          </Button>
        </form>
      </section>
    </>
  );
};

export default Posteditor;
