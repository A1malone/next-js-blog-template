import React from "react";
import Layout from "../../components/Layout";

function Blogpost({ blogposts }) {
  return (
    <>
      {
        <Layout
          key={blogposts[0]._id}
          title={blogposts[0].title}
          description={blogposts[0].shortDescription}
          url={blogposts[0].meta.url}
          header={blogposts[0].title}
          author={blogposts[0].author}
          published={blogposts[0].publishedOn}
          seo={"hi"}
          head={"hello"}
          layout={"goodbye"}
        >
          {blogposts[0].body}
        </Layout>
      }
    </>
  );
}

// This function gets called at build time
export async function getStaticPaths() {
  // Call an external API endpoint to get posts
  const res = await fetch("https://alfredmalone.com/blog/FetchPosts");
  let blogpaths = await res.json();

  // Get the paths we want to pre-render based on posts
  let paths = blogpaths.map((path) => ({
    params: { id: path.blogPath },
  }));
  // We'll pre-render only these paths at build time.
  // { fallback: false } means other routes should 404.
  return { paths, fallback: false };
}

// This also gets called at build time
export async function getStaticProps({ params }) {
  // params contains the post `id`.
  // If the route is like /posts/1, then params.id is 1
  const res = await fetch(`https://alfredmalone.com/blog/FetchPosts`);
  const postdata = await res.json();
  if (postdata === null) {
    return false, console.log("post data not found");
  }

  const blogData = postdata.filter((post) => post.blogPath === params.id);
  // Pass post data to the page via props
  return {
    props: {
      blogposts: blogData,
    },
  };
}

export default Blogpost;
