import "bootstrap/dist/css/bootstrap.min.css";
import "../sass/main.scss";

export default function MyApp({ Component, pageProps }) {
  return <Component {...pageProps} />;
}
