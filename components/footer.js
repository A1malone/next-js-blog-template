import React from "react";

const Footer = (props) => {
  return (
    <>
      <footer className="container-fluid p-2 mt-auto blue-bg">
        <div className="copyright-footer">
          © 2020 All rights reserved. Alfred Malone.
        </div>
      </footer>
    </>
  );
};

export default Footer;
