import React from "react";
import Link from "next/link";
import Post from "./BlogUtilities/post";

const testpost = [
  {
    title: "This is an example title for this example blog post",
    image: "/assets/2.jpg",
    publishedOn: "January 20, 2020",
    body: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.",
  },
  {
    title: "This is an example title for this example blog post",
    image: "/assets/3.jpg",
    publishedOn: "January 20, 2020",
    body: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.",
  },
  {
    title: "This is an example title for this example blog post",
    image: "/assets/4.jpg",
    publishedOn: "January 20, 2020",
    body: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.",
  },
  {
    title: "This is an example title for this example blog post",
    image: "/assets/3.jpg",
    publishedOn: "January 20, 2020",
    body: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.",
  },
  {
    title: "This is an example title for this example blog post",
    image: "/assets/2.jpg",
    publishedOn: "January 20, 2020",
    body: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.",
  },
  {
    title: "This is an example title for this example blog post",
    image: "/assets/4.jpg",
    publishedOn: "January 20, 2020",
    body: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.",
  },
];

const Controlposts = (props) => {
  return (
    <>
      {props.control ? (
        <section className="container-fluid blog-padding">
          <div className="row">
            {testpost.map((post, i) => (
              <div className="col-sm-12 col-md-6 col-lg-4" key={i}>
                <Link href="/exampleblogpost">
                  <a className="blog-link">
                    <Post
                      image={post.image}
                      title={post.title}
                      date={post.publishedOn}
                      body={post.body}
                    />
                  </a>
                </Link>
              </div>
            ))}
          </div>
        </section>
      ) : (
        console.log("Their are no posts to display")
      )}
    </>
  );
};

export default Controlposts;
