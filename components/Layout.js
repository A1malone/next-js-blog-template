import React from "react";
import Nav from "./navbar";
import Seo from "./SEO";
import Header from "./BlogUtilities/header";
import Filler from "./BlogUtilities/layout";
import Footer from "./footer";

const Blogpost = (props) => {
  return (
    <>
      <Seo
        title={props.title}
        Description={props.description}
        url={props.url}
      />
      <Nav />
      <Header
        header={props.header}
        author={props.author}
        published={props.published}
      />
      <main dangerouslySetInnerHTML={{ __html: props.children }}></main>
      {/*<Filler />*/}
      <Footer />
    </>
  );
};

export default Blogpost;
