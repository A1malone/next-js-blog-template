import React from "react";
import Navbar from "react-bootstrap/Navbar";
import NavDropdown from "react-bootstrap/NavDropdown";
import Link from "next/link";

const Navigation = () => {
  return (
    <>
      <Navbar className="white-bg sticky-top p-0" expand="lg">
        <div className="container navbar navbar-expand-lg navbar-light sticky-top p-0">
          <Link href="/">
            <a className="navbar-brand d-flex flex-row align-items-center p-2">
              <img src="/brand.svg" width="80" height="80" alt="brand" />
            </a>
          </Link>
          <Navbar.Toggle aria-controls="basic-navbar-nav" />
          <Navbar.Collapse id="basic-navbar-nav">
            <div className="ms-auto d-flex align-items-center">
              <img
                height="40"
                width="40"
                src="/assets/user-circle-regular.svg"
              />
              <NavDropdown title="User Name" id="basic-nav-dropdown">
                <Link href="/">
                  <NavDropdown.Item href="/">Home</NavDropdown.Item>
                </Link>
                <Link href="/examplePostEditor">
                  <NavDropdown.Item href="/examplePostEditor">
                    Post Editor
                  </NavDropdown.Item>
                </Link>
                <Link href="/exampleblogpost">
                  <NavDropdown.Item href="/exampleblogpost">
                    Example Blog Post
                  </NavDropdown.Item>
                </Link>
                <Link href="/blog/test">
                  <NavDropdown.Item href="/blog/test">
                    Dynamic link
                  </NavDropdown.Item>
                </Link>
              </NavDropdown>
            </div>
          </Navbar.Collapse>
        </div>
      </Navbar>
    </>
  );
};

export default Navigation;
