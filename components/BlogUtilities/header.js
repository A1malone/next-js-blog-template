import React from "react";
import Image from "next/image";

const Blogheader = (props) => {
  return (
    <>
      <div className="container post-margin">
        <h1 className="post-header">{props.header}</h1>
        <div className="post-subsection">
          <div className="post-publication">
            <span className="author">{props.author}</span>
            <span className="post-d-t">{props.published} · 5 min read</span>
          </div>
          <div className="social-media">
            <img
              src="/assets/icons/twitter-brands.svg"
              alt="Social_Media"
              className="social-media-icon"
            />
            <img
              src="/assets/icons/linkedin-brands.svg"
              alt="Social_Media"
              className="social-media-icon mx-2"
            />
            <img
              src="/assets/icons/facebook-square-brands.svg"
              alt="Social_Media"
              className="social-media-icon"
            />
          </div>
        </div>
      </div>
    </>
  );
};

export default Blogheader;
