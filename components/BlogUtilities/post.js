import React from "react";
import Image from "next/image";

const Post = (props) => {
  return (
    <>
      <article className="d-flex flex-column mt-5">
        <Image
          src={props.image}
          alt="Blog_Post_Photo"
          height="100%"
          width="100%"
          className="blog-post-img"
          layout="responsive"
        />
        <div className="blog-post-body">
          <span className="blog-post-text">{props.date}</span>
          <h2 className="blog-post-header">{props.title}</h2>
          <p className="blog-post-text">{props.body}</p>
        </div>
      </article>
    </>
  );
};

export default Post;
