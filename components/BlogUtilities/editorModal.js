import React from "react";

const Emodal = (props) => {
  return (
    <>
      <button
        type="button"
        className="btn btn-primary"
        data-bs-toggle="modal"
        data-bs-target="#exampleModal"
      >
        Launch demo modal
      </button>
      <div
        className="modal fade"
        id="exampleModal"
        tabIndex="-1"
        aria-labelledby="exampleModalLabel"
        aria-hidden="true"
      >
        <div className="modal-dialog">
          <div className="modal-content">
            <div className="modal-header">
              <h5 className="modal-title" id="exampleModalLabel">
                Post Blog
              </h5>
              <button
                type="button"
                className="btn-close"
                data-bs-dismiss="modal"
                aria-label="Close"
              ></button>
            </div>
            <div className="modal-body">
              <div className="form-floating mb-3">
                <input
                  required
                  type="text"
                  className="form-control"
                  id="blogTitle"
                  placeholder="title"
                  name="title"
                />
                <label htmlFor="blogTitle">Title</label>
              </div>

              <div className="form-floating mb-3">
                <input
                  type="text"
                  className="form-control"
                  id="blogAuthor"
                  placeholder="Author"
                  name="author"
                />
                <label htmlFor="blogAuthor">Author</label>
              </div>

              <div className="form-floating mb-3">
                <textarea
                  required
                  className="form-control"
                  placeholder="Leave a comment here"
                  id="floatingTextarea2"
                  style={{ height: "100px" }}
                  name="shortDescripti}on"
                />
                <label htmlFor="floatingTextarea2">Short Description</label>
              </div>

              <div className="input-group mb-3">
                <span className="input-group-text" id="basic-addon3">
                  /
                </span>
                <div className="form-floating">
                  <input
                    type="text"
                    className="form-control"
                    id="blogTitle"
                    placeholder="url"
                    name="url"
                  />
                  <label htmlFor="url">Custom Blog Path</label>
                </div>
              </div>
            </div>
            <div className="modal-footer">
              <button type="submit" className="btn btn-primary">
                Submit
              </button>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default Emodal;
