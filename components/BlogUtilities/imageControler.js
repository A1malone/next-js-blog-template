import React from "react";
import Image from "next/image";

const Imagecontroler = () => {
  return (
    <>
      <div className="post-img-fill">
        <Image
          alt="Mountains"
          src="/assets/1.jpg"
          layout="fill"
          objectFit="cover"
        />
      </div>
    </>
  );
};

export default Imagecontroler;
