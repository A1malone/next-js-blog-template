import React from "react";
import Imagecontroler from "./imageControler";
import Body from "./body";

const Blogbody = () => {
  return (
    <>
      <Imagecontroler />
      <Body>
        <div className="container post-mb-5">
          <h2 className="post-subHeader">
            <strong>Header 1 about this post to display the work of the</strong>
          </h2>
          <p className="post-body">
            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
            eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim
            ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut
            aliquip ex ea commodo consequat. Duis aute irure dolor in
            reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla
            pariatur. Excepteur sint occaecat cupidatat non proident, sunt in
            culpa qui officia deserunt mollit anim id est laborum.
          </p>
        </div>
        <div className="container-fluid post-mb-5">
          <h2 className="post-subHeader">
            <strong>Header 2</strong>
          </h2>
          <div className="row">
            <div className="col-sm post-mb-4-sm">
              <img
                alt="post-img"
                height="100%"
                width="100%"
                className="img-radius"
                src="/assets/2.jpg"
              />
            </div>
            <div className="col-sm">
              <p className="post-body">
                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
                eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut
                enim ad minim veniam, quis nostrud exercitation ullamco laboris
                nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor
                in reprehenderit in voluptate velit esse cillum dolore eu fugiat
                nulla pariatur. Excepteur sint occaecat cupidatat non proident,
                sunt in culpa qui officia deserunt mollit anim id est laborum.
              </p>
            </div>
          </div>
        </div>
        <div className="container">
          <h2 className="post-subHeader">
            <strong>Header 3</strong>
          </h2>
          <div className="row mb-3">
            <div className="col-sm post-mb-4-sm">
              <img
                alt="post-img"
                height="100%"
                width="100%"
                className="img-radius"
                src="/assets/3.jpg"
              />
            </div>
            <div className="col-sm">
              <img
                alt="post-img"
                height="100%"
                width="100%"
                className="img-radius"
                src="/assets/4.jpg"
              />
            </div>
          </div>
          <p className="post-body">
            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
            eiusmod tempor incididunt ut labore et dolore magna aliqua. Gravida
            cum sociis natoque penatibus et magnis dis. Mattis aliquam faucibus
            purus in massa tempor nec feugiat. Mollis aliquam ut porttitor leo a
            diam sollicitudin tempor id. Consectetur a erat nam at lectus urna
            duis convallis convallis. Nibh ipsum consequat nisl vel pretium
            lectus quam. Blandit libero volutpat sed cras. Id semper risus in
            hendrerit. Sed faucibus turpis in eu mi bibendum neque. Ultrices
            vitae auctor eu augue ut lectus. Pulvinar sapien et ligula
            ullamcorper malesuada. Vitae suscipit tellus mauris a diam maecenas
            sed enim ut. In fermentum posuere urna nec tincidunt praesent semper
            feugiat. At varius vel pharetra vel turpis nunc eget lorem.
            Phasellus egestas tellus rutrum tellus pellentesque eu tincidunt
            tortor. A lacus vestibulum sed arcu non odio euismod lacinia at. Sem
            integer vitae justo eget magna fermentum iaculis eu non. Vitae
            tortor condimentum lacinia quis vel eros. Amet justo donec enim diam
            vulputate.
          </p>
          <h3 className="post-subsection-header d-flex justify-content-end">
            <strong>Post Section Header 1</strong>
          </h3>
          <p className="post-body">
            Mollis nunc sed id semper risus in hendrerit gravida. Dictum fusce
            ut placerat orci nulla pellentesque. Id nibh tortor id aliquet
            lectus proin nibh nisl condimentum. Interdum velit euismod in
            pellentesque massa placerat duis. Pulvinar pellentesque habitant
            morbi tristique senectus et netus et malesuada. Libero nunc
            consequat interdum varius. Feugiat nisl pretium fusce id velit ut.
            Eu sem integer vitae justo eget magna fermentum iaculis. Volutpat
            odio facilisis mauris sit amet massa vitae. Nisl nunc mi ipsum
            faucibus vitae aliquet nec. Enim nec dui nunc mattis enim ut.
          </p>
          <p className="post-body">
            Nunc sed id semper risus in. Enim facilisis gravida neque convallis
            a cras semper auctor. Eget velit aliquet sagittis id consectetur
            purus ut faucibus. Tincidunt augue interdum velit euismod in
            pellentesque massa placerat. Dictum non consectetur a erat. In est
            ante in nibh. Volutpat blandit aliquam etiam erat. Nulla porttitor
            massa id neque. Viverra accumsan in nisl nisi scelerisque. Varius
            sit amet mattis vulputate. Vitae congue eu consequat ac felis donec
            et odio. Ipsum dolor sit amet consectetur adipiscing elit
            pellentesque habitant morbi. Convallis a cras semper auctor neque
            vitae. Senectus et netus et malesuada fames. Libero volutpat sed
            cras ornare arcu dui. Vitae tortor condimentum lacinia quis.
          </p>
          <h3 className="post-subsection-header">
            <strong>Post Section Header 2</strong>
          </h3>
          <p className="post-body">
            Facilisis magna etiam tempor orci eu. Morbi tristique senectus et
            netus et malesuada fames. Tincidunt ornare massa eget egestas purus
            viverra accumsan in nisl. Ornare lectus sit amet est. Sed adipiscing
            diam donec adipiscing tristique risus nec. Ac felis donec et odio
            pellentesque diam volutpat commodo sed. Eget mauris pharetra et
            ultrices neque ornare aenean. Risus nullam eget felis eget nunc
            lobortis mattis aliquam faucibus. Urna duis convallis convallis
            tellus id interdum velit. Purus faucibus ornare suspendisse sed nisi
            lacus. Massa vitae tortor condimentum lacinia quis vel eros donec
            ac. Diam sit amet nisl suscipit adipiscing bibendum est ultricies.
            Lacus vel facilisis volutpat est velit egestas dui. Sem integer
            vitae justo eget magna fermentum iaculis eu. Auctor eu augue ut
            lectus arcu bibendum at varius vel. Leo in vitae turpis massa sed.
            Augue eget arcu dictum varius duis at consectetur lorem. Turpis
            cursus in hac habitasse. Facilisis gravida neque convallis a cras
            semper auctor. Congue quisque egestas diam in arcu cursus euismod.
          </p>
          <p className="post-body">
            Amet aliquam id diam maecenas ultricies mi eget. In est ante in nibh
            mauris cursus mattis. Tincidunt id aliquet risus feugiat in ante. A
            cras semper auctor neque. Eu mi bibendum neque egestas congue
            quisque. Nisi porta lorem mollis aliquam. Elit pellentesque habitant
            morbi tristique senectus et netus et malesuada. A iaculis at erat
            pellentesque adipiscing commodo elit at imperdiet. Ullamcorper eget
            nulla facilisi etiam dignissim diam. Quam nulla porttitor massa id
            neque aliquam vestibulum morbi blandit. Nunc aliquet bibendum enim
            facilisis gravida. Metus dictum at tempor commodo ullamcorper a
            lacus vestibulum. Purus non enim praesent elementum. Pellentesque
            pulvinar pellentesque habitant morbi.
          </p>
        </div>
      </Body>
    </>
  );
};

export default Blogbody;
