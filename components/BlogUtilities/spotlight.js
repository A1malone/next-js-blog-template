import React from "react";
import Link from "next/link";
import Image from "next/image";

const Spotlight = (props) => {
  return (
    <>
      <Link href="/blog/test">
        <a className="blog-link">
          <article className="row mt-5">
            <div className="col-sm-12 col-lg-7">
              <img
                src={props.image}
                alt="Spotlight_Photo"
                layout="fill"
                className="blog-spotlight-img img-radius-shadow"
              />
            </div>
            <div className="col-sm-12 col-lg-5 blog-spotlight-body">
              <span className="blog-spotlight-text">{props.date}</span>
              <h2 className="blog-spotlight-header">{props.title}</h2>
              <p className="blog-spotlight-text">{props.body}</p>
            </div>
          </article>
        </a>
      </Link>
    </>
  );
};

export default Spotlight;
