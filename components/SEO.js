import React from "react";
import Head from "next/head";

const favi = "/brand.svg";

const SEO = (props) => {
  return (
    <Head>
      <title>{props.title}</title>
      {/* Primary Meta Tags */}
      <meta name="title" content={props.title} />
      <link rel="shortcut icon" href={favi} />
      <meta name="description" content={props.description} />
      {/* Open Graph / Facebook */}
      <meta property="og:type" content="website" />
      <meta property="og:url" content={props.url} />
      <meta property="og:title" content={props.title} />
      <meta property="og:description" content={props.description} />
      <meta property="og:image" content={favi} />
      {/* Twitter */}
      <meta property="twitter:card" content="summary_large_image" />
      <meta property="twitter:url" content={props.url} />
      <meta property="twitter:title" content={props.title} />
      <meta property="twitter:description" content={props.description} />
      <meta property="twitter:image" content={favi} />
    </Head>
  );
};

export default SEO;
